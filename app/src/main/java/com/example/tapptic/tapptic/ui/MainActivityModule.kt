package com.example.tapptic.tapptic.ui

import com.example.tapptic.tapptic.ui.list.FragmentList
import com.example.tapptic.tapptic.ui.list.detail.FragmentDetails
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector
    internal abstract fun itemFragment(): FragmentList

    @ContributesAndroidInjector
    internal abstract fun detailFragment(): FragmentDetails
}