package com.example.tapptic.tapptic.ui.list.detail

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations.map
import android.arch.lifecycle.Transformations.switchMap
import android.arch.lifecycle.ViewModel
import com.example.tapptic.core.database.entity.TappticItemDetail
import com.example.tapptic.core.domain.Resource
import com.example.tapptic.core.domain.Status
import com.example.tapptic.core.repository.TappticRepository
import com.example.tapptic.core.utils.livedata.SingleLiveEvent
import com.example.tapptic.tapptic.ui.model.Item
import com.example.tapptic.tapptic.ui.model.ItemDetail
import com.shopify.livedataktx.*
import javax.inject.Inject

class DetailViewModel @Inject internal constructor(tappticRepository: TappticRepository): ViewModel() {

    private val refresh = SingleLiveEvent<String>()

    private val apiResponse = switchMap(refresh) { tappticRepository.getTappticDetailItem(it) }

    val loadingStatus: LiveData<Status> = map(apiResponse.nonNull().distinct()) { it.status }

    val detailsData: LiveData<ItemDetail> = map(apiResponse.nonNull().filter { it.data != null }.distinct()) { response ->
        response.data?.let { ItemDetail(it.name, it.text, it.imageUrl) } }

   fun forceRefresh(name: String){
       refresh.value = name
   }


}