package com.example.tapptic.tapptic.ui.list

import android.os.Bundle
import androidx.navigation.fragment.findNavController
import com.example.tapptic.core.domain.Status

import com.example.tapptic.tapptic.R
import com.example.tapptic.tapptic.databinding.FragmentItemListBinding
import com.example.tapptic.tapptic.ui.BindingFragment
import com.example.tapptic.tapptic.ui.bundleOf
import com.example.tapptic.tapptic.ui.inTransaction
import com.example.tapptic.tapptic.ui.list.adapter.ItemListAdapter
import com.example.tapptic.tapptic.ui.model.Item
import com.shopify.livedataktx.observe
import com.example.tapptic.tapptic.ui.list.detail.FragmentDetails
import com.example.tapptic.tapptic.ui.list.detail.FragmentDetails.Companion.NAME
import com.example.tapptic.tapptic.ui.newFragmentInstance
import com.orhanobut.logger.Logger
import com.shopify.livedataktx.distinct
import com.shopify.livedataktx.nonNull
import android.support.v7.widget.LinearLayoutManager



open class FragmentList : BindingFragment<FragmentItemListBinding, ListViewModel>() {

    private val adapter = ItemListAdapter(::handleItemClick)

    private val recycleViewViewModel by lazy { provideVMWithActivityOwner<RecycleViewViewModel>() }

    private val linearLayoutManager: LinearLayoutManager
     get() = (binding.includeList.itemList.layoutManager as LinearLayoutManager)

    override val layoutId: Int = R.layout.fragment_item_list

    override fun initViewModel() = provideVMWithFragmentOwner<ListViewModel>()

    override fun onBoundView(savedInstanceState: Bundle?) {

        viewModel.refresh.call()
        adapter.isTabletLand = isTabletLand

        binding.includeList.itemList.adapter = adapter
        viewModel.itemsLiveData.nonNull().distinct().observe(this) {
            adapter.setContentList(it)

            if (isTabletLand && adapter.isNotEmpty()) {
                val position = viewModel.currentItem
                val item = adapter.getItem(position)
                handleItemClick(position, item)
            }
        }

        viewModel.loadingStatus.nonNull().distinct().observe(this) {
            when (it) {
                Status.ERROR -> viewModel.refresh.call()
                Status.LOADING -> Logger.d("Trying to connect")
                else -> {
                }
            }
        }
        adapter.lastSelectedId = recycleViewViewModel.selectedPosition
        binding.includeList.itemList.post {
            linearLayoutManager.onRestoreInstanceState(recycleViewViewModel.listState)
            linearLayoutManager.scrollToPosition(recycleViewViewModel.selectedPosition)
        }
    }

    override fun onSaveInstanceState(state: Bundle) {
        super.onSaveInstanceState(state)
        recycleViewViewModel.listState = linearLayoutManager.onSaveInstanceState()
    }

    private fun handleItemClick(position: Int, item: Item) {
        recycleViewViewModel.selectedPosition = position
        bundleOf(NAME to item.name).also {
            if (!isTabletLand) {
                findNavController().navigate(R.id.fragmentDetails, it)
            } else {
                childFragmentManager.inTransaction { replace(R.id.details_container, newFragmentInstance<FragmentDetails>(it)) }
            }

        }

        viewModel.currentItem = position
    }
}