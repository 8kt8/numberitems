package com.example.tapptic.tapptic.ui.list.adapter

import android.support.annotation.NonNull
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.tapptic.tapptic.R
import com.example.tapptic.tapptic.databinding.ListItemViewBinding

import com.example.tapptic.tapptic.ui.model.Item

class ListViewHolder(val binding: ListItemViewBinding) : RecyclerView.ViewHolder(binding.root) {


    fun bind(item: Item, isSelected: Boolean) {
        binding.mainLayout.isSelected = isSelected
        binding.tvName.text = item.name
        Glide.with(binding.root.context).load(item.imageUrl).into(binding.ivImage)
    }
}