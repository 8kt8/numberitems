package com.example.tapptic.tapptic.di;


import com.example.tapptic.tapptic.ui.MainActivity;
import com.example.tapptic.tapptic.ui.MainActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class ActivityModule {
	@ContributesAndroidInjector(modules = MainActivityModule.class)
	abstract MainActivity mainActivity();



}

