package com.example.tapptic.tapptic.ui

import android.content.res.Configuration
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.tapptic.core.internetConnection.NetworkService
import com.example.tapptic.tapptic.R

import com.example.tapptic.tapptic.databinding.MainActivityBinding
import com.shopify.livedataktx.*
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : BindingActivity<MainActivityBinding>() {

    private val navigator: NavController
        get() = findNavController(nav_host_fragment)

    override val layoutId = R.layout.main_activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!isTabletLand) {
            setupActionBarWithNavController(navigator)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home && !isTabletLand) {
            popNavigatorBackStack()
        }
        return super.onOptionsItemSelected(item)
    }

    fun popNavigatorBackStack() = navigator.popBackStack()
}