package com.example.tapptic.tapptic.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.v7.widget.RecyclerView;

import com.example.tapptic.tapptic.ui.list.ListViewModel;
import com.example.tapptic.tapptic.ui.list.RecycleViewViewModel;
import com.example.tapptic.tapptic.ui.list.detail.DetailViewModel;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import dagger.Binds;
import dagger.MapKey;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ListViewModel.class)
    abstract ViewModel mainListViewModel(ListViewModel listViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel.class)
    abstract ViewModel detailViewModel(DetailViewModel detailViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(RecycleViewViewModel.class)
    abstract ViewModel recycleViewViewModel(RecycleViewViewModel recycleViewViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(CoreViewModelFactory factory);

    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @MapKey
    @interface ViewModelKey {
        Class<? extends ViewModel> value();
    }

}

