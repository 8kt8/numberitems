package com.example.tapptic.tapptic.ui.list

import android.arch.lifecycle.ViewModel
import android.os.Parcelable
import javax.inject.Inject

class RecycleViewViewModel @Inject internal constructor(): ViewModel(){

    var listState: Parcelable? = null

    var selectedPosition = 0

}