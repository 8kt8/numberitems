package com.example.tapptic.tapptic.ui.list

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations.map
import android.arch.lifecycle.Transformations.switchMap
import android.arch.lifecycle.ViewModel
import android.os.Parcelable
import android.util.Pair
import com.example.tapptic.core.domain.Status
import com.example.tapptic.core.internetConnection.NetworkService
import com.example.tapptic.core.repository.TappticRepository
import com.example.tapptic.core.utils.livedata.SingleLiveEvent
import com.example.tapptic.core.utils.livedata.callAdapter.combineLatest
import com.example.tapptic.tapptic.ui.model.Item
import com.shopify.livedataktx.distinct
import com.shopify.livedataktx.filter
import com.shopify.livedataktx.map
import com.shopify.livedataktx.nonNull
import javax.inject.Inject

class ListViewModel @Inject internal constructor(tappticRepository: TappticRepository): ViewModel() {

    val refresh: SingleLiveEvent<Void> = SingleLiveEvent()

    private val apiResponse = switchMap(refresh) { tappticRepository.getTappticItems() }

    var currentItem: Int = 0

    val itemsLiveData: LiveData<List<Item>> = map(apiResponse.nonNull().distinct()) { response ->
        response.data?.map { Item(it.name, it.imageUrl) } }

    val loadingStatus: LiveData<Status> = map(apiResponse.nonNull().distinct())  { it.status }


}