package com.example.tapptic.tapptic.ui

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.example.tapptic.tapptic.R

import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


abstract class BindingActivity<BindingView : ViewDataBinding> : AppCompatActivity(), HasSupportFragmentInjector {

	protected val isTabletLand: Boolean
		get() = resources.getBoolean(R.bool.isTabletLand)

	@Inject
	lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

	//TODO add private set, actually proguard failed this is the JetBrains bug
	protected lateinit var binding: BindingView


	protected abstract val layoutId: Int

	override fun onCreate(savedInstanceState: Bundle?) {
		AndroidInjection.inject(this)
		super.onCreate(savedInstanceState)
		binding = DataBindingUtil.setContentView(this, layoutId)
		binding.setLifecycleOwner(this)
	}

	override fun supportFragmentInjector() = dispatchingAndroidInjector
}