package com.example.tapptic.tapptic.di;

import android.app.Application;


import com.example.tapptic.core.di.CoreModule;
import com.example.tapptic.tapptic.common.AppApplication;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Component(modules = {AndroidInjectionModule.class, AppModule.class, CoreModule.class, ActivityModule.class, ViewModelModule.class})
@Singleton
public interface AppComponent {

	void inject(AppApplication appApplication);

	@Component.Builder
	interface Builder {
		@BindsInstance
		Builder application(Application application);

		AppComponent build();
	}
}
