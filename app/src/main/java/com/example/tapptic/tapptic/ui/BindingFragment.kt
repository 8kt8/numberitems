package com.example.tapptic.tapptic.ui

//import com.example.tapptic.BR
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.tapptic.core.internetConnection.NetworkService
import com.example.tapptic.tapptic.R
import com.shopify.livedataktx.distinct
import com.shopify.livedataktx.map
import com.shopify.livedataktx.nonNull
import com.shopify.livedataktx.observe
import javax.inject.Inject


abstract class BindingFragment<BindingView : ViewDataBinding, VM : ViewModel> : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var networkService: NetworkService

    protected val isTabletLand: Boolean
        get() = resources.getBoolean(R.bool.isTabletLand)

    //TODO add private set, actually proguard failed this is the JetBrains bug
    protected lateinit var binding: BindingView

    protected abstract val layoutId: Int

    protected val viewModel: VM by lazy { initViewModel() }

    abstract fun initViewModel(): VM

    protected abstract fun onBoundView(savedInstanceState: Bundle?)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        observeConnectionState()

        return DataBindingUtil.inflate<BindingView>(inflater, layoutId, container, false).apply {
            //setVariable(BR.viewModel, viewModel)
            setLifecycleOwner(this@BindingFragment)
            binding = this
            onBoundView(savedInstanceState)
        }.root
    }

    private fun observeConnectionState() {
        networkService.connectionReceiver.nonNull()
                .distinct()
                .map { it.isConnected }
                .observe(this) {
                    if (!it) {
                        Toast.makeText(requireContext(), R.string.offlineBanner_noInternetConnection, Toast.LENGTH_SHORT).show()
                    }
                }
    }

    override fun onDestroyView() {
        binding.unbind()
        super.onDestroyView()
    }

    protected inline fun <reified VM : ViewModel> provideVMWithActivityOwner() =
            ViewModelProviders.of(requireActivity(), viewModelFactory).get(VM::class.java)

    // By default, the method tries to return a ViewModel with ParentFragment as lifecycle owner.
    // parentFragment is not null only when parent fragment use childFragmentManger for transactions
    // If the fragment doesn't have a parent, it uses this.
    // To get a separate viewModel for a fragment that has a parent, pass this in the method call
    protected inline fun <reified VM : ViewModel> provideVMWithFragmentOwner(owner: Fragment = parentFragment
            ?: this) = ViewModelProviders.of(owner, viewModelFactory).get(VM::class.java)

    fun popNavigatorBackStack() = (activity as MainActivity?)?.popNavigatorBackStack()

}