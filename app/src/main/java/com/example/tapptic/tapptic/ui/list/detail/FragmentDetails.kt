package com.example.tapptic.tapptic.ui.list.detail

import android.content.res.Configuration
import android.os.Bundle
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.tapptic.core.domain.Status
import com.example.tapptic.tapptic.R
import com.example.tapptic.tapptic.databinding.ItemDetailBinding
import com.example.tapptic.tapptic.ui.BindingFragment
import com.example.tapptic.tapptic.ui.MainActivity
import com.orhanobut.logger.Logger
import com.shopify.livedataktx.debounce
import com.shopify.livedataktx.distinct
import com.shopify.livedataktx.nonNull
import com.shopify.livedataktx.observe

class FragmentDetails : BindingFragment<ItemDetailBinding, DetailViewModel>() {

    override val layoutId = R.layout.item_detail

    override fun initViewModel() = provideVMWithFragmentOwner<DetailViewModel>()

    override fun onBoundView(savedInstanceState: Bundle?) {

        if (savedInstanceState != null && isTabletLand) {
            popNavigatorBackStack()
        } else {
            forceRefresh()

            viewModel.detailsData.nonNull()
                    .distinct()
                    .observe(this) {
                            binding.tvName.text = it.name
                            binding.tvText.text = it.text
                            Glide.with(this).load(it.imageUrl).into(binding.ivImage)
                    }
        }

        viewModel.loadingStatus.nonNull().distinct().observe(this) {
            when (it) {
                Status.ERROR -> forceRefresh()
                Status.LOADING -> Logger.d("Trying to connect")
                else -> {
                }
            }
        }
    }

    private fun forceRefresh() {
        arguments?.getString(NAME)?.let {
            viewModel.forceRefresh(it)
        }
    }

    companion object {
        const val NAME = "NAME"
    }

}