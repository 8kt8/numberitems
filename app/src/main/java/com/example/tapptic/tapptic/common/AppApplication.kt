package com.example.tapptic.tapptic.common

import android.app.Activity
import android.app.Application
import com.example.tapptic.core.utils.CoreExecutors
import com.example.tapptic.tapptic.di.AppComponent
import com.example.tapptic.tapptic.di.AppInjector
import com.example.tapptic.tapptic.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

open class AppApplication : Application(), HasActivityInjector {

	@Inject
	lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

	protected open val appComponent: AppComponent by lazy {
		DaggerAppComponent.builder().application(appApplication).build()
	}

	private val appApplication: AppApplication
		get() = this

	override fun onCreate() {
		AppInjector.init(appComponent, appApplication)
		super.onCreate()
	}


	override fun activityInjector(): AndroidInjector<Activity> {
		return dispatchingAndroidInjector
	}
}
