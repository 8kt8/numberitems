package com.example.tapptic.tapptic.ui.model


data class Item(val name: String, val imageUrl: String)

data class ItemDetail(val name: String, val text: String, val imageUrl: String)