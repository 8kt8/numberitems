package com.example.tapptic.tapptic.ui.list.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.tapptic.tapptic.R.bool.isTabletLand
import com.example.tapptic.tapptic.databinding.ListItemViewBinding
import com.example.tapptic.tapptic.ui.model.Item


class ItemListAdapter(private val clickConsumer: (Int, Item) -> Unit) : RecyclerView.Adapter<ListViewHolder>() {

    private var contentList: List<Item> = listOf()

    var lastSelectedId = 0
    var isTabletLand = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : ListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        ListItemViewBinding.inflate(layoutInflater, parent, false)
                .run { return ListViewHolder(this) }
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val item = contentList[position]
        val isSelected = (position == lastSelectedId && isTabletLand)
        holder.bind(item, isSelected)
        holder.binding.root.setOnClickListener{ _ ->
            clickConsumer.invoke(position, item)
            val oldSelectedId = lastSelectedId
            lastSelectedId = position
            notifyItemChanged(oldSelectedId)
            notifyItemChanged(lastSelectedId)
        }
    }

    override fun getItemCount() = contentList.size

    fun isNotEmpty() = contentList.isNotEmpty()

    fun getItem(position: Int) = contentList[position]

    fun setContentList(contentList: List<Item>?) {
        contentList?.let {
            this.contentList = it
            notifyDataSetChanged()
        }

    }

}
