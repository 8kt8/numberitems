package com.example.tapptic.core.repository.mapper

import com.example.tapptic.core.api.tapptick.TapptickItemDetailResponse
import com.example.tapptic.core.api.tapptick.TapptickItemResponse
import com.example.tapptic.core.database.entity.TappticItem
import com.example.tapptic.core.database.entity.TappticItemDetail
import javax.inject.Inject

class TappticItemMapper @Inject internal constructor() {

    fun map(response: List<TapptickItemResponse>) = response.asSequence()
            .filter { validateItem(it) }
            .map { TappticItem(it.name, it.imagePath) }
            .toList()

    fun mapDetails(response: TapptickItemDetailResponse) =
            if(validateDetailItem(response)){
                TappticItemDetail(response.name, response.text, response.imagePath)
            } else {
                null
            }



    fun validateItem(item: TapptickItemResponse): Boolean  {
        if(item.imagePath == null) return false
        if(item.name == null) return false
        return true
    }

    fun validateDetailItem(item: TapptickItemDetailResponse): Boolean  {
        if(item.imagePath == null) return false
        if(item.name == null) return false
        if(item.text == null) return false
        return true
    }
}