package com.example.tapptic.core.internetConnection

data class ConnectionModel(val type: Int, val isConnected: Boolean)