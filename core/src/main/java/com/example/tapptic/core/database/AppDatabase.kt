package com.example.tapptic.core.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.tapptic.core.database.dao.TappticItemDao
import com.example.tapptic.core.database.dao.TappticItemDetailsDao
import com.example.tapptic.core.database.entity.TappticItem
import com.example.tapptic.core.database.entity.TappticItemDetail


@Database(entities = [TappticItem::class, TappticItemDetail::class], version = AppDatabase.DATABASE_VERSION)
abstract class AppDatabase : RoomDatabase() {

    abstract fun itemDao(): TappticItemDao

    abstract fun itemDetailDao(): TappticItemDetailsDao

    companion object {

        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "tapptic.db"
    }
}
