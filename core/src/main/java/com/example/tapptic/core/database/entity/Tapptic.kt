package com.example.tapptic.core.database.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class TappticItem(@field:PrimaryKey val name: String, val imageUrl: String)

@Entity
data class TappticItemDetail(@field:PrimaryKey val name: String, val text: String, val imageUrl: String)
