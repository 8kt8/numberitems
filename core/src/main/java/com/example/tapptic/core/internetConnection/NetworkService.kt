package com.example.tapptic.core.internetConnection

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkService @Inject internal constructor(val connectionReceiver: ConnectionReceiver, private val context: Context) {

	val isConnected: Boolean
		get() = connectionReceiver.value?.isConnected ?: isNetworkConnected(context)
}

fun getNetworkInfo(context: Context): NetworkInfo? {
	val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
		as ConnectivityManager
	return connectivityManager.activeNetworkInfo
}

fun isNetworkConnected(context: Context) = getNetworkInfo(context)?.isConnected ?: false