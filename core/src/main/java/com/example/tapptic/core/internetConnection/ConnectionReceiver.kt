package com.example.tapptic.core.internetConnection

import android.arch.lifecycle.LiveData
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.ConnectivityManager.TYPE_MOBILE
import android.net.ConnectivityManager.TYPE_WIFI
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ConnectionReceiver @Inject constructor(private val context: Context) : LiveData<ConnectionModel>() {

	private val networkReceiver = object : BroadcastReceiver() {
		override fun onReceive(context: Context, intent: Intent) {
			intent.extras?.let {
				if (isNetworkConnected(context)) {
					when (getNetworkInfo(context)?.type) {
						TYPE_WIFI -> postValue(ConnectionModel(TYPE_WIFI, true))
						TYPE_MOBILE -> postValue(ConnectionModel(TYPE_MOBILE, true))
					}
				} else {
					postValue(ConnectionModel(NO_INTERNET, false))
				}
			}
		}
	}

	override fun onActive() {
		super.onActive()
		val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
		context.registerReceiver(networkReceiver, filter)
	}

	override fun onInactive() {
		super.onInactive()
		context.unregisterReceiver(networkReceiver)
	}

	companion object {
		const val NO_INTERNET = -1
	}
}


