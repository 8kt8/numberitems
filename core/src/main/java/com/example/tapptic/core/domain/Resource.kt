package com.example.tapptic.core.domain

/**
 * A generic class that holds a value with its loading status.
 */
data class Resource<T>(val status: Status, val data: T?, val message: String?, val code: Int) {

    companion object {

        private const val DEFAULT_ERROR = -1
        private const val NO_ERROR = 0

        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null, NO_ERROR)
        }

        fun <T> error(msg: String, data: T?, errorCode: Int): Resource<T> {
            assert(errorCode != NO_ERROR)
            return Resource(Status.ERROR, data, msg, errorCode)
        }

        fun <T> error(msg: String, data: T?): Resource<T> {
            return Resource(Status.ERROR, data, msg, DEFAULT_ERROR)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null, NO_ERROR)
        }
    }
}
