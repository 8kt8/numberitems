package com.example.tapptic.core.utils.livedata.callAdapter

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData

fun <A, B> zip(a: LiveData<A>, b: LiveData<B>): LiveData<Pair<A, B>> {
    return MediatorLiveData<Pair<A, B>>().apply {
        var lastA: A? = null
        var lastB: B? = null

        fun update() {
            val localLastA = lastA
            val localLastB = lastB

            ifNotNull(localLastA, localLastB) { a, b -> this.value = Pair(a, b) }
        }

        addSource(a) {
            lastA = it
            update()
        }
        addSource(b) {
            lastB = it
            update()
        }
    }
}

fun <A, B, C> combineLatest(a: LiveData<A>, b: LiveData<B>, func: (A, B) -> C): LiveData<C> {

    return MediatorLiveData<C>().apply {

        addSource(zip(a, b)) { pair ->
            pair?.let {
                func(it.first, it.second)?.let { c ->
                    this.value = c
                }
            }
        }
    }
}


inline fun <A, B, R> ifNotNull(a: A?, b: B?, code: (A, B) -> R) {
    if (a != null && b != null) {
        code(a, b)
    }
}