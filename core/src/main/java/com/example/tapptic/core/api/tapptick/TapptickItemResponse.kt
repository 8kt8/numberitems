package com.example.tapptic.core.api.tapptick

import com.google.gson.annotations.SerializedName

data class TapptickItemResponse(
        @SerializedName("name") val name: String,
        @SerializedName("image") val imagePath: String
)

data class TapptickItemDetailResponse(
        @SerializedName("name") val name: String,
        @SerializedName("image") val imagePath: String,
        @SerializedName("text") val text: String
)
