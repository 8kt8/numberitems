package com.example.tapptic.core.repository


import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.support.annotation.MainThread
import android.support.annotation.WorkerThread
import com.example.tapptic.core.api.ApiEmptyResponse
import com.example.tapptic.core.api.ApiErrorResponse
import com.example.tapptic.core.api.ApiResponse
import com.example.tapptic.core.api.ApiSuccessResponse
import com.example.tapptic.core.domain.Resource
import com.example.tapptic.core.utils.CoreExecutors
import com.orhanobut.logger.Logger

/**
 * A generic class that can provide a resource backed by both the sqlite database and the network.
 *
 *
 * You can read more about it in the [Architecture
 * Guide](https://developer.android.com/arch).
 * @param <ResultType>
 * @param <RequestType>
</RequestType></ResultType> */
abstract class NetworkBoundResource<ResultType, RequestType>
@MainThread constructor(private val coreExecutors: CoreExecutors) {

    private val result = MediatorLiveData<Resource<ResultType>>()

    init {
        result.value = Resource.loading(null)
        @Suppress("LeakingThis")
        val dbSource = loadFromDb()
        result.addSource(dbSource) { data ->
            result.removeSource(dbSource)
            if (shouldFetch(data)) {
                fetchFromNetwork(dbSource)
            } else {
                result.addSource(dbSource) { newData ->
                    setValue(Resource.success(newData))
                }
            }
        }
    }

    @MainThread
    private fun setValue(newValue: Resource<ResultType>) {
        if (result.value != newValue) {
            result.value = newValue
        }
    }

    private fun fetchFromNetwork(dbSource: LiveData<ResultType>) {
        val apiResponse = createCall()
        result.addSource(dbSource) { newData ->
            setValue(Resource.loading(newData))
        }
        result.addSource(apiResponse) { response ->
            result.removeSource(apiResponse)
            result.removeSource(dbSource)
            when (response) {
                is ApiSuccessResponse -> {
                    coreExecutors.networkIO.execute {
                        val processedResponse = processResponse(response)
                        Logger.d("${javaClass.name} Processed response $processedResponse")
                        val convertedResponse = convertResponse(processedResponse)
                        Logger.d("${javaClass.name} Converted response $convertedResponse")
                        coreExecutors.discIO.execute {
                            convertedResponse?.let {
                                saveCallResult(processedResponse, convertedResponse)
                                Logger.d("${javaClass.name} saveCallResult")
                            }

                            coreExecutors.mainThread.execute {
                                result.addSource(loadFromDb()) { newData ->
                                    setValue(Resource.success(newData))
                                }
                            }
                        }
                    }
                }
                is ApiEmptyResponse -> {
                    coreExecutors.mainThread.execute {
                        // reload from disk whatever we had
                        result.addSource(loadFromDb()) { newData ->
                            setValue(Resource.success(newData))
                        }
                    }
                }
                is ApiErrorResponse -> {
                    onFetchFailed()
                    result.addSource(dbSource) { newData ->
                        setValue(Resource.error(response.errorMessage, newData))
                    }
                }
            }
        }
    }

    protected open fun onFetchFailed() {}

    fun asLiveData() = result as LiveData<Resource<ResultType>>

    @WorkerThread
    protected open fun processResponse(response: ApiSuccessResponse<RequestType>) = response.body

    @WorkerThread
    protected abstract fun saveCallResult(response: RequestType, result: ResultType)

    @WorkerThread
    abstract fun convertResponse(response: RequestType): ResultType?

    @MainThread
    protected open fun shouldFetch(data: ResultType?): Boolean = true

    @MainThread
    protected abstract fun loadFromDb(): LiveData<ResultType>

    @MainThread
    protected abstract fun createCall(): LiveData<ApiResponse<RequestType>>
}
