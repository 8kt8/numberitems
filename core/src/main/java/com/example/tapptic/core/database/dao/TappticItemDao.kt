package com.example.tapptic.core.database.dao


import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.example.tapptic.core.database.AppDatabase
import com.example.tapptic.core.database.entity.TappticItem


@Dao
abstract class TappticItemDao protected constructor(private val db: AppDatabase) {

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	abstract fun insertAll(items: List<TappticItem>): Array<Long>

	@Transaction
	@Query("SELECT * FROM tappticitem")
	abstract fun getAllItems(): LiveData<List<TappticItem>>
}
