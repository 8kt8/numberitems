package com.example.tapptic.core.api.interceptor

import com.example.tapptic.core.internetConnection.NetworkService
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import javax.inject.Inject

class ConnectivityInterceptor @Inject
constructor(private val networkService: NetworkService) : Interceptor {

	@Throws(IOException::class)
	override fun intercept(chain: Interceptor.Chain): Response {
		return if (!networkService.isConnected) {
			throw NoConnectionException()
		} else {
			chain.proceed(chain.request())
		}
	}
}

class NoConnectionException : IOException() {
	override val message = "No Internet Connection"
}