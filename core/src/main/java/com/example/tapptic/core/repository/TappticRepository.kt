package com.example.tapptic.core.repository

import android.arch.lifecycle.LiveData
import com.example.tapptic.core.api.tapptick.TapptickApi
import com.example.tapptic.core.api.tapptick.TapptickItemDetailResponse
import com.example.tapptic.core.api.tapptick.TapptickItemResponse
import com.example.tapptic.core.database.dao.TappticItemDao
import com.example.tapptic.core.database.dao.TappticItemDetailsDao
import com.example.tapptic.core.database.entity.TappticItem
import com.example.tapptic.core.database.entity.TappticItemDetail
import com.example.tapptic.core.domain.Resource
import com.example.tapptic.core.repository.mapper.TappticItemMapper
import com.example.tapptic.core.utils.CoreExecutors
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TappticRepository @Inject internal constructor(private val executors: CoreExecutors,
                     private val tappticItemDao: TappticItemDao,
                     private val tappticItemDetailsDao: TappticItemDetailsDao,
                     private val tapptickApi: TapptickApi,
                     private val tappticItemMapper: TappticItemMapper) {

    fun getTappticItems(): LiveData<Resource<List<TappticItem>>> {
        return object : NetworkBoundResource<List<TappticItem>, List<TapptickItemResponse>>(executors) {

            override fun convertResponse(response: List<TapptickItemResponse>) = tappticItemMapper.map(response)

            override fun saveCallResult(response: List<TapptickItemResponse>, result: List<TappticItem>) { tappticItemDao.insertAll(result) }

            override fun loadFromDb() = tappticItemDao.getAllItems()

            override fun createCall() = tapptickApi.getData()

        }.asLiveData()
    }

    fun getTappticDetailItem(name: String): LiveData<Resource<TappticItemDetail>> {
        return object : NetworkBoundResource<TappticItemDetail, TapptickItemDetailResponse>(executors) {

            override fun convertResponse(response: TapptickItemDetailResponse) = tappticItemMapper.mapDetails(response)

            override fun saveCallResult(response: TapptickItemDetailResponse, result: TappticItemDetail) { tappticItemDetailsDao.insertAll(result) }

            override fun loadFromDb() = tappticItemDetailsDao.getItem(name)

            override fun createCall() = tapptickApi.getItemDetails(name)

        }.asLiveData()
    }
}
