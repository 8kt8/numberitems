package com.example.tapptic.core.di

import android.arch.persistence.room.Room
import android.content.Context
import com.example.tapptic.core.BuildConfig
import com.example.tapptic.core.api.tapptick.TapptickApi
import com.example.tapptic.core.api.interceptor.ConnectivityInterceptor
import com.example.tapptic.core.database.AppDatabase
import com.example.tapptic.core.utils.livedata.callAdapter.LiveDataCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class CoreModule {

    private val API_URL = "http://dev.tapptic.com/test/"

    @Provides
    @Singleton
    @Named("standardClient")
    internal fun standardClient(cache: Cache,
                                @Named("debugMode") debugMode: Boolean,
                                connectivityInterceptor: ConnectivityInterceptor): OkHttpClient {
        val builder = OkHttpClient.Builder()
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        if (debugMode) {
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        } else {
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.NONE
        }
        return builder
                .addInterceptor(connectivityInterceptor)
                .addInterceptor(httpLoggingInterceptor)
                .cache(cache)
                .readTimeout(30000, TimeUnit.MILLISECONDS)
                .build()
    }

    @Provides
    @Singleton
    @Named("debugMode")
    internal fun isDebugMode(): Boolean {
        return BuildConfig.DEBUG
    }

    @Provides
    @Singleton
    internal fun okhttpCache(context: Context): Cache {
        val cacheSize = 10 * 1024 * 1024 // 10 MB
        return Cache(context.cacheDir, cacheSize.toLong())
    }

    @Provides
    @Singleton
    @Named("standardRetrofit")
    internal fun defaultRetrofit(@Named("standardClient") okHttpClient: OkHttpClient) = createStandardRetrofit(okHttpClient, API_URL)


    private fun createStandardRetrofit(okHttpClient: OkHttpClient, apiUrl: String): Retrofit {
        return Retrofit.Builder()
                .baseUrl(apiUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .build()
    }

    @Provides
    @Singleton
    internal fun tapptickApi(@Named("standardRetrofit") retrofit: Retrofit) = retrofit.create<TapptickApi>(TapptickApi::class.java)

    @Provides
    @Singleton
    internal fun appDatabase(context: Context) = Room.databaseBuilder(context, AppDatabase::class.java, AppDatabase.DATABASE_NAME).fallbackToDestructiveMigration().build()

    @Provides
    @Singleton
    internal fun itemDao(db: AppDatabase) = db.itemDao()

    @Provides
    @Singleton
    internal fun itemDetailDao(db: AppDatabase) = db.itemDetailDao()


}