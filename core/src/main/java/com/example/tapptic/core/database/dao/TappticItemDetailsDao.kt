package com.example.tapptic.core.database.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.example.tapptic.core.database.AppDatabase
import com.example.tapptic.core.database.entity.TappticItemDetail

@Dao
abstract class TappticItemDetailsDao protected constructor(private val db: AppDatabase) {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(items: TappticItemDetail): Long

    @Transaction
    @Query("SELECT * FROM tappticitemdetail")
    abstract fun getAllItems(): LiveData<List<TappticItemDetail>>

    @Transaction
    @Query("SELECT * FROM tappticitemdetail WHERE name==:name")
    abstract fun getItem(name: String): LiveData<TappticItemDetail>
}