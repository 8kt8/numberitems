package com.example.tapptic.core.api.tapptick

import android.arch.lifecycle.LiveData
import com.example.tapptic.core.api.ApiResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface TapptickApi {
    @GET("json.php")
    fun getData(): LiveData<ApiResponse<List<TapptickItemResponse>>>

    @GET("json.php?name=")
    fun getItemDetails(@Query("name") name: String): LiveData<ApiResponse<TapptickItemDetailResponse>>
}