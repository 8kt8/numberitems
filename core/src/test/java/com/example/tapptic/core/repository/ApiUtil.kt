package com.example.tapptic.core.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.example.tapptic.core.api.ApiResponse
import okhttp3.ResponseBody
import retrofit2.Response

object ApiUtil {

    fun <T> successCall(data: T): LiveData<ApiResponse<T>> {
        return createCall(Response.success(data))
    }

    fun <T> errorCall(errorNumber: Int, responseBody: ResponseBody): LiveData<ApiResponse<T>> {
        return createCall(Response.error(errorNumber, responseBody))
    }

    fun <T> createCall(response: Response<T>): LiveData<ApiResponse<T>> {
        val data = MutableLiveData<ApiResponse<T>>()
        data.value = ApiResponse.create(response)
        return data
    }

}