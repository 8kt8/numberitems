package com.example.tapptic.core.repository

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import com.example.tapptic.core.InstantAppExecutors
import com.example.tapptic.core.api.tapptick.TapptickApi
import com.example.tapptic.core.api.tapptick.TapptickItemDetailResponse
import com.example.tapptic.core.api.tapptick.TapptickItemResponse
import com.example.tapptic.core.database.dao.TappticItemDao
import com.example.tapptic.core.database.dao.TappticItemDetailsDao
import com.example.tapptic.core.database.entity.TappticItem
import com.example.tapptic.core.database.entity.TappticItemDetail
import com.example.tapptic.core.domain.Resource
import com.example.tapptic.core.repository.mapper.TappticItemMapper
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.After
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.junit.Rule
import org.mockito.Mockito

class TappticRepositoryTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    companion object {
        @JvmStatic
        private val itemsDao = mockk<TappticItemDao>(relaxed = true)
        @JvmStatic
        private val itemDetailsDao = mockk<TappticItemDetailsDao>(relaxed = true)
        @JvmStatic
        private val api = mockk<TapptickApi>()
        @JvmStatic
        private val itemMapper = mockk<TappticItemMapper>(relaxed = true)
        @JvmStatic
        private val systemUnderTest = TappticRepository(InstantAppExecutors(), itemsDao, itemDetailsDao, api, itemMapper)
    }

    @Test
    fun getTappticItems() {
        val dbData = MutableLiveData<List<TappticItem>>()
        every { itemsDao.getAllItems() } returns dbData
        val observer = mockk<Observer<Resource<List<TappticItem>>>>(relaxed = true)

        systemUnderTest.getTappticItems().observeForever(observer)
        val response = ApiUtil.successCall(generateItems())
        every { api.getData() } returns response

        systemUnderTest.getTappticItems().observeForever(observer)
        val updatedDbData = MutableLiveData<List<TappticItem>>()
        every { itemsDao.getAllItems() } returns updatedDbData
        dbData.value = null

        every { itemsDao.getAllItems() } returns updatedDbData
        dbData.value = null
        verify { api.getData() }
    }

    @Test
    fun getTappticDetailItem() {
        val dbData = MutableLiveData<TappticItemDetail>()
        every { itemDetailsDao.getItem("a") } returns dbData
        val observer = mockk<Observer<Resource<TappticItemDetail>>>(relaxed = true)

        systemUnderTest.getTappticDetailItem("a").observeForever(observer)
        val response = ApiUtil.successCall(generateDetailItem())
        every { api.getItemDetails("a") } returns response

        systemUnderTest.getTappticDetailItem("a").observeForever(observer)
        val updatedDbData = MutableLiveData<TappticItemDetail>()
        every { itemDetailsDao.getItem("a") } returns updatedDbData
        dbData.value = null

        every { itemDetailsDao.getItem("a") } returns updatedDbData
        dbData.value = null
        verify { api.getItemDetails("a") }
    }

    private fun generateItems() = listOf(TapptickItemResponse("a", "b"))
    private fun generateDetailItem() = TapptickItemDetailResponse("a", "c", "b")

}