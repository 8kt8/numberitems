package com.example.tapptic.core

import com.example.tapptic.core.utils.CoreExecutors
import java.util.concurrent.Executor

class InstantAppExecutors : CoreExecutors(instant, instant, instant, instant) {
    companion object {

        private val instant = Executor { it.run() }
    }
}