# NumberItems

1.App have two modules core(data base, services, repositories) and app(Ui part)

2.Recycle view for displaying list elements, states of rows are defined in xml files in drawable directory. 
On tablet in landscape orientation clicked item change state to selected.

3.No internet connection error is visible when app lost internet connection or when user change fragment. 
If call to service return error result, app try to reconnect as long as service return success status in response.

4.In general the code was written in Kotlin, di package in app module was written in Java.

5.I increased min sdk to 16 because now 99,6 percent of all devices on the market have sdk 16+. I made this decision becouse I needed one library liveDataktx(Kotlin extension for LiveData, chaining like RxJava) I prefer to use LiveData and Coroutines(in this project I using executors, due to coroutines wasn't on libraries list) then more complicated, complex and heavier rxJava.

6.App have MVVM architecture with Google Architecture Components.

7.I used Dagger 2 for dependency injection.

8.For testing I changed Mockito to Mockk because this is more kotlin friendly library and give the same and more possibilities. I was written test for TappticRepository class, com.example.tapptic.core.repository is package of created tests.

9.Logs are added to debug build by dagger, for dispaly logs I add orhanobut library.
 
10.I additionally used the room to save the downloaded data in the database and displayed last dowloaded data, in case when user haven't internet connection.

11 Project contains two simple product flavours a, b and two build type debug and release. 


